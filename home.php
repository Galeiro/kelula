<?php

session_start();
if (!isset($_SESSION["login"])) {
    header("Location: index.php");
    exit;
}

require 'functions.php';

$date_expense = query("SELECT DATE_FORMAT(date_expense,'%M %Y') AS tanggal FROM expense WHERE id_user = '$id_user' GROUP BY YEAR (date_expense), MONTH (date_expense) ORDER BY YEAR (date_expense) DESC, MONTH (date_expense) DESC");
$id_user = $_SESSION["id_user"];
$budget = query("SELECT * FROM budget WHERE id_user = '$id_user'")[0];

$filterIncome = "";
if (isset($_POST["filterIncome"]) && !empty($_POST["incomeCategoryFilter"])) {
    $filterIncome = " AND code_category_income = '" . $_POST["incomeCategoryFilter"] . "'";
    $limitIncome = "";
} else {
    $limitIncome = " LIMIT 6"; 
}
if (isset($_POST["monthFilter"]) && !empty($_POST["monthFilterIncome"])) {
    $filterIncome .= " AND DATE_FORMAT(date_income,'%M %Y') = '" . $_POST["monthFilterIncome"] . "'";
}
$incomes = query("SELECT * FROM income JOIN category_income USING(code_category_income) WHERE id_user = '$id_user' $filterIncome ORDER BY (date_income) DESC, (code_income) DESC$limitIncome");

$filterExpense = "";
if (isset($_POST["filterExpense"]) && !empty($_POST["expenseCategory"])) {
    $filterExpense = " AND code_category_expense = '" . $_POST["expenseCategory"] . "'";
    $limitExpense = "";

    // Add month filter
    if (isset($_POST["monthFilter"]) && !empty($_POST["monthFilter"])) {
        $filterExpense .= " AND DATE_FORMAT(date_expense,'%M %Y') = '" . $_POST["monthFilter"] . "'";
    }
} else {
    $limitExpense = " LIMIT 6"; 
}
$expenses = query("SELECT * FROM expense JOIN category_expense USING(code_category_expense) WHERE id_user = '$id_user' $filterExpense ORDER BY (date_expense) DESC, (code_expense) DESC$limitExpense");

if (isset($_POST["viewIncome"])) {
    $incomes = query("SELECT * FROM income JOIN category_income USING(code_category_income) WHERE id_user = '$id_user' ORDER BY (date_income) DESC");
}
if (isset($_POST["viewExpense"])) {
    $expenses = query("SELECT * FROM expense JOIN category_expense USING(code_category_expense) WHERE id_user = '$id_user' ORDER BY (date_expense) DESC");
}

$budget_income = query("SELECT SUM(budget_income) AS budget_income FROM income WHERE id_user = '$id_user' $filterIncome")[0];
$budget_expense = query("SELECT SUM(budget_expense) AS budget_expense FROM expense WHERE id_user = '$id_user' $filterExpense")[0];

if (isset($_POST["submitBudget"])) {

    if (setBudget($_POST) > 0) {
        echo "

      <script>
        alert('Set budget successfully');
        document.location.href = 'home.php';
      </script>

    ";
    } else {

        echo "

      <script>
        alert('Set budget failed');
      </script>

    ";
    }
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="img/logo.png">

    <title>Kelula - Beranda</title>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light d-none d-md-block bg-light fixed-top">
        <div class="container-md">
            <a class="navbar-brand" href="home.php">
                <img src="img/logo.png" alt="" width="35" height="35" class="me-2"><span class="fw-bold">Kelula</span>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto">
                    <a class="nav-link actives" href="home.php">Beranda</a>
                    <a class="nav-link" href="analysis.php">Rangkuman</a>
                    <a class="nav-link" href="goal.php">Target</a>
                    <a class="nav-link" href="loan.php">Pinjaman</a>
                    <a class="nav-link" href="profile.php">Akun</a>
                </div>
            </div>
        </div>
    </nav>
    <!-- Bottom Navbar -->
    <nav class="navbar navbar-light bg-light border-top navbar-expand d-md-none d-lg-none d-xl-none fixed-bottom">
        <ul class="navbar-nav nav-justified w-100">
            <li class="nav-item">
                <a href="home.php" class="nav-link active">
                    <span>
                        <i class="fas fa-home"></i>
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="analysis.php" class="nav-link">
                    <span>
                        <i class="fas fa-chart-area"></i>
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="goal.php" class="nav-link">
                    <span>
                        <i class="fas fa-bullseye"></i>
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="loan.php" class="nav-link">
                    <span>
                        <i class="fas fa-money-check-alt"></i>
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="profile.php" class="nav-link">
                    <span>
                        <i class="fas fa-user"></i>
                    </span>
                </a>
            </li>
        </ul>
    </nav>

    <section id="budget" class="budget">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 mt-3">
                    <div class="card balance">
                        <div class="card-body">
                            <p class="card-text text-center">Saldo</p>
                            <h3 class="card-text mt-3 text-center"><b>IDR
                                    <?= number_format($budget["money"], 2, ',', '.'); ?></b> <button type="button"
                                    class="btn btn-light btn-sm ms-1" data-bs-toggle="modal"
                                    data-bs-target="#noteSetBudget"><i class="fas fa-plus"></i></button></h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text text-center">Pemasukan</p>
                            <h3 class="card-text mt-3 text-success text-center"><b><i
                                        class="fas fa-angle-double-up"></i> IDR
                                    <?= number_format($budget_income["budget_income"], 2, ',', '.'); ?></b></h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text text-center">Pengeluaran</p>
                            <h3 class="card-text mt-3 text-danger text-center"><b><i
                                        class="fas fa-angle-double-down"></i> IDR
                                    <?= number_format($budget_expense["budget_expense"], 2, ',', '.'); ?></b></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="filter" class="filter">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST">
                                <div class="row">
                                    <p class="card-text text-center mt-3">Filter Pemasukan</p>
                                    <div class="col-6">
                                        <select class="form-select" id="incomeCategoryFilter"
                                            name="incomeCategoryFilter">
                                            <option value=""
                                                <?php echo isset($_POST['incomeCategoryFilter']) && $_POST['incomeCategoryFilter'] === '' ? 'selected' : ''; ?>>
                                                Kategori</option>
                                            <?php 
                                                $incomeCategories = query("SELECT * FROM category_income");
                                                foreach ($incomeCategories as $incomeCategory) {
                                                    $selected = isset($_POST['incomeCategoryFilter']) && $_POST['incomeCategoryFilter'] === $incomeCategory['code_category_income'] ? 'selected' : '';
                                                    echo "<option value='" . $incomeCategory['code_category_income'] . "' $selected>" . $incomeCategory['name_category_income'] . "</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <select name="monthFilterIncome" id="monthFilterIncome" class="form-select">
                                            <option value="">Bulan</option>
                                            <?php 
                                            $date_income = query("SELECT DATE_FORMAT(date_income, '%M %Y') AS tanggal 
                                                                    FROM income 
                                                                    WHERE id_user = '$id_user' 
                                                                    GROUP BY YEAR(date_income), MONTH(date_income) 
                                                                    ORDER BY YEAR(date_income) DESC, MONTH(date_income) DESC");

                                            foreach ($date_income as $date_inc) : ?>
                                            <option value="<?= htmlspecialchars($date_inc["tanggal"]); ?>"
                                                <?php echo ($_POST['monthFilterIncome'] === $date_inc["tanggal"]) ? 'selected' : ''; ?>>
                                                <?= htmlspecialchars($date_inc["tanggal"]); ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col mt-3">
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-success" type="submit"
                                                name="filterIncome">Filter</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <p class="card-text text-center mt-3">Filter Pengeluaran</p>
                                    <div class="col-6">
                                        <select class="form-select" id="expenseCategory" name="expenseCategory">
                                            <option value=""
                                                <?php echo isset($_POST['expenseCategory']) && $_POST['expenseCategory'] === '' ? 'selected' : ''; ?>>
                                                Kategori</option>
                                            <?php 
                                                $categories = query("SELECT * FROM category_expense");
                                                foreach ($categories as $category) {
                                                    $selected = isset($_POST['expenseCategory']) && $_POST['expenseCategory'] === $category['code_category_expense'] ? 'selected' : '';
                                                    echo "<option value='" . $category['code_category_expense'] . "' $selected>" . $category['name_category_expense'] . "</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <select name="monthFilter" id="monthFilter" class="form-select">
                                            <option value="">Bulan</option>
                                            <?php 
                                            $date_expense = query("SELECT DATE_FORMAT(date_expense, '%M %Y') AS tanggal 
                                                                    FROM expense 
                                                                    WHERE id_user = '$id_user' 
                                                                    GROUP BY YEAR(date_expense), MONTH(date_expense) 
                                                                    ORDER BY YEAR(date_expense) DESC, MONTH(date_expense) DESC");

                                            foreach ($date_expense as $date_exp) : ?>
                                            <option value="<?= htmlspecialchars($date_exp["tanggal"]); ?>"
                                                <?php echo ($_POST['monthFilter'] === $date_exp["tanggal"]) ? 'selected' : ''; ?>>
                                                <?= htmlspecialchars($date_exp["tanggal"]); ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col mt-3">
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-danger" type="submit"
                                                name="filterExpense">Filter</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="incomes" class="incomes mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>Pemasukan</h3>
                </div>
                <div class="col text-end">
                    <a href="addIncome.php" class="btn btn-success"><i class="fas fa-angle-double-up"></i></a>
                </div>
            </div>
            <div class="row list mt-3">
                <?php if (count($incomes) >= 1) : ?>
                <?php foreach ($incomes as $income) : ?>
                <div class="col-xl-6 mb-3">
                    <div class="card list-incomes"
                        onclick="document.location.href = 'detailIncome.php?code_income=<?= $income['code_income']; ?>';">
                        <div class="card-body">
                            <div class="row d-flex align-items-center">
                                <div class="col-2 text-center">
                                    <span>
                                        <i class="fas fa-<?= $income['icon_category_income']; ?>"></i>
                                    </span>
                                </div>
                                <div class="col-5">
                                    <h6 class="card-text text-start"><?= $income['name_category_income']; ?></h6>
                                    <?php $tgl = date_create($income['date_income']); ?>
                                    <p class="card-text text-start text-secondary" style="font-size: 14px;">
                                        <?= date_format($tgl, 'd F Y') ?></p>
                                    <p class="card-text text-start text-secondary" style="font-size: 14px;">
                                        <?= $income['note_income']; ?></p>
                                </div>
                                <div class="col-5">
                                    <p class="card-text text-success text-end"><b>IDR
                                            <?= number_format($income['budget_income'], 2, ',', '.'); ?></b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php if (!isset($_POST["viewIncome"]) && count($incomes) >= 2 && empty($filterIncome)) : ?>
                <form action="" method="post" class="text-center">
                    <button class="mt-3 btn btn-link text-dark" type="submit" name="viewIncome">Lihat semua
                        pemasukan</button>
                </form>
                <?php endif; ?>
                <?php else : ?>
                <div class="col-xl-12 mt-3">
                    <div class="card list-incomes empty" onclick="document.location.href = 'addIncome.php';">
                        <div class="card-body text-center mt-3">
                            <span style="font-size: 50px;">
                                <i class="fas fa-angle-double-up"></i>
                            </span>
                            <h3 class="card-title mt-4">Pemasukan tidak ada</h3>
                            <p class="text-secondary">Wooops! Data pemasukan tidak ditemukan. Anda dapat membuat
                                pemasukan baru.</p>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section id="expense" class="expense mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>Pengeluaran</h3>
                </div>
                <div class="col text-end">
                    <a href="addExpense.php" class="btn btn-danger"><i class="fas fa-angle-double-down"></i></a>
                </div>
            </div>
            <div class="row list mt-3">
                <?php if (count($expenses) >= 1) : ?>
                <?php foreach ($expenses as $expense) : ?>
                <div class="col-xl-6 mb-3">
                    <div class="card list-expense"
                        onclick="document.location.href = 'detailExpense.php?code_expense=<?= $expense['code_expense']; ?>';">
                        <div class="card-body">
                            <div class="row d-flex align-items-center">
                                <div class="col-2 text-center">
                                    <span>
                                        <i class="fas fa-<?= $expense['icon_category_expense']; ?>"></i>
                                    </span>
                                </div>
                                <div class="col-5">
                                    <h6 class="card-text text-start"><?= $expense['name_category_expense']; ?></h6>
                                    <?php $tgl = date_create($expense['date_expense']); ?>
                                    <p class="card-text text-start text-secondary" style="font-size: 14px;">
                                        <?= date_format($tgl, 'd F Y') ?></p>
                                    <p class="card-text text-start text-secondary" style="font-size: 14px;">
                                        <?= $expense['note_expense']; ?></p>
                                </div>
                                <div class="col-5">
                                    <p class="card-text text-danger text-end"><b>IDR
                                            <?= number_format($expense['budget_expense'], 2, ',', '.'); ?></b></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php if (!isset($_POST["viewExpense"]) && count($expenses) >= 2 && empty($filterExpense)) : ?>
                <form action="" method="post" class="text-center">
                    <button class="mt-3 btn btn-link text-dark" type="submit" name="viewExpense">Lihat semua
                        pengeluaran</button>
                </form>
                <?php endif; ?>
                <?php else : ?>
                <div class="col-xl-12 mt-3">
                    <div class="card list-expense empty" onclick="document.location.href = 'addExpense.php';">
                        <div class="card-body text-center mt-3">
                            <span style="font-size: 50px;">
                                <i class="fas fa-angle-double-down"></i>
                            </span>
                            <h3 class="card-title mt-4">Pengeluaran tidak ada</h3>
                            <p class="text-secondary">Wooops! Data pengeluaran tidak ditemukan. Anda dapat membuat
                                pengeluaran baru.</p>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <div class="modal fade" id="noteSetBudget" class="noteSetBudget" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalToggleLabel">Catatan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p class="justify">Silahkan masukan saldo Anda dengan benar.<span class="fw-bold"> Data Anda akan
                            terhapus ketika melakukan perubahan saldo</span>.</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-bs-target="#setBudget" data-bs-toggle="modal">Buat
                        saldo</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="setBudget" class="setBudget" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tetapkan Saldo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" method="post">
                    <div class="modal-body">
                        <input type="hidden" class="form-control" id="code_budget" name="code_budget"
                            value="<?= $budget["code_budget"]; ?>">
                        <input type="hidden" class="form-control" id="id_user" name="id_user"
                            value="<?= $budget["id_user"]; ?>">
                        <div class="mb-3">
                            <label for="inputBudget" class="col-form-label">Saldo</label>
                            <input type="number" class="form-control" id="inputBudget" name="inputBudget" autofocus
                                required placeholder="6000000">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submitBudget" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

</html>