## Kelula
Kelula merupakan aplikasi pengelolaan keuangan yang dirancang untuk membantu Anda mengelola uang Anda dengan lebih efektif.

Kelula merupakan aplikasi berbasis open source hasil fork dari <a href="https://github.com/zcode25/fuluss">Fuluss</a> yang dikembangkan oleh <a href="https://www.instagram.com/zcode25/">ZCODE.

## Teknologi
- HTML
- CSS
- JavaScript
- PHP
- Bootstrap 5

## Pengembangan

### Tanpa Docker
1. Pastikan, Web Server (PHP) dan Database Server sudah terinstall.
2. Klon repo ini.
    ```
    git clone https://gitlab.com/Galeiro/kelula.git
    ```
3. Buat **.env** dari **.env.example**.
3. Sesuikan variabel **.env** dengan kebutuhan Anda.
4. Impor database awal dari **./docker/database/budget_tracker.sql** ke database Anda.
5. Letakan seluruh folder ini di direktori root pada web server Anda.

### Dengan Docker
1. Klon repo ini.
    ```
    git clone https://gitlab.com/Galeiro/kelula.git
    ```
2. Buat **.env** dari **.env.example**.
3. Sesuaikan **.env** sesuai kemauan Anda.
2. Jalankan.
    ```
    docker compose up -d
    ```