<?php

session_start();
if (isset($_SESSION["login"])) {
    header("Location: home.php");
    exit;
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="img/logo.png">

    <title>Kelula</title>
</head>

<body>


    <section id="main" class="main">
        <div class="container">
            <div class="row">
                <div class="col mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title fw-bolder">Kebijakan Privasi Kelula</h1>
                            <p class="card-text mt-3 mb-4 justify">Kelula merupakan aplikasi pengelolaan keuangan yang
                                dirancang untuk membantu Anda mengelola uang Anda dengan lebih efektif.</p>
                            <p class="card-text mt-3 mb-4 justify">Kelula merupakan aplikasi berbasis open source hasil
                                fork dari <a href="https://github.com/zcode25/fuluss">Fuluss</a> yang dikembangkan oleh
                                <a href="https://www.instagram.com/zcode25/">ZCODE.</a></p>
                            <a href="#" class="btn btn-outline-success" target="_blank">Kebijakan Privasi</a>
                            <hr>
                            <p style="font-size: 14px;" class="card-text text-secondary">Production by <span
                                    class="text-dark fw-bold">Musex</span></p>
                            <p style="font-size: 14px;" class="card-text text-secondary">© 2023 Kelula. All rights
                                reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

</html>