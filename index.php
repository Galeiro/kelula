<?php

session_start();
if (isset($_SESSION["login"])) {
    header("Location: home.php");
    exit;
}

require 'functions.php';
if (isset($_POST["submit"])) {

    $email_user = $_POST["email_user"];
    $pass_user = sha1($_POST["pass_user"]);

    $result = mysqli_query($conn, "SELECT * FROM user WHERE email_user = '$email_user'");

    if (mysqli_num_rows($result) === 1) {
        $row = mysqli_fetch_assoc($result);

        if ($pass_user == $row["pass_user"]) {

            $_SESSION["id_user"] = $row["id_user"];
            $_SESSION["name_user"] = $row["name_user"];
            $_SESSION["login"] = true;

            header("Location: home.php");
            exit;
        }
    }

    $error = true;
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="img/logo.png">

    <title>Kelula</title>
</head>

<body>


    <section id="main" class="main">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title fw-bolder">Kelula</h1>
                            <p class="card-text mt-3 mb-4 justify">Kelula merupakan aplikasi pengelolaan keuangan yang
                                dirancang untuk membantu Anda mengelola uang Anda dengan lebih efektif.</p>
                            <p class="card-text mt-3 mb-4 justify">Kelula merupakan aplikasi berbasis open source hasil
                                fork dari <a href="https://github.com/zcode25/fuluss">Fuluss</a> yang dikembangkan oleh
                                <a href="https://www.instagram.com/zcode25/">ZCODE.</a></p>
                            <!-- <a href="#" class="btn btn-success" target="_blank">Follow</a> -->
                            <a href="#" class="btn btn-outline-success" data-bs-toggle="modal"
                                data-bs-target="#privacyPolicyModal">Kebijakan Privasi</a>
                            <hr>
                            <p style="font-size: 14px;" class="card-text text-secondary">Production by <span
                                    class="text-dark fw-bold">Musex</span></p>
                            <p style="font-size: 14px;" class="card-text text-secondary">© 2023 Kelula. All rights
                                reserved.</p>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="privacyPolicyModal" tabindex="-1" role="dialog"
                    aria-labelledby="privacyPolicyModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="privacyPolicyModalLabel">Kebijakan Privasi Kelula</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <h5>Pendahuluan</h5>
                                <p class="card-text mt-3 mb-4 justify">Selamat datang di Kelula, aplikasi pengelolaan
                                    keuangan berbasis open source. Kebijakan privasi ini menjelaskan bagaimana kami
                                    mengumpulkan, menggunakan, dan melindungi informasi pribadi Anda saat Anda
                                    menggunakan aplikasi Kelula. Kami berkomitmen untuk melindungi privasi pengguna dan
                                    memberikan pengalaman pengguna yang aman.</p>
                                <h5>Keamanan Informasi</h5>
                                <p class="card-text mt-3 mb-4 justify">Kami menggunakan tindakan keamanan teknis dan
                                    organisasi untuk melindungi informasi pribadi pengguna. Setiap password pengguna
                                    akan dienkripsi, dan data pengguna disimpan dengan standar keamanan kontrol yang
                                    tinggi.</p>
                                <h5>Berbagi Informasi dengan Pihak Ketiga</h5>
                                <p class="card-text mt-3 mb-4 justify">Kami tidak akan menjual, menyewakan, atau
                                    membagikan informasi pribadi pengguna kepada pihak ketiga tanpa izin Anda, kecuali
                                    yang diizinkan atau diwajibkan oleh hukum.</p>
                                <h5>Penghapusan Informasi</h5>
                                <p class="card-text mt-3 mb-4 justify">Anda dapat meminta penghapusan akun dan informasi
                                    Anda dari Kelula kapan saja. Kami akan menghapus informasi sesuai dengan kebijakan
                                    retensi data kami.</p>
                                <h5>Kontak</h5>
                                <p class="card-text mt-3 mb-4 justify">Jika Anda memiliki pertanyaan, usulan, atau kekhawatiran
                                    terkait kelula, silakan hubungi kami ke galih@musex.my.id.</p>
                                <p class="card-text mt-3 mb-4 justify"> Dengan menggunakan aplikasi Kelula, Anda
                                    menyetujui kebijakan privasi ini. Terima kasih atas kepercayaan Anda menggunakan
                                    Kelula untuk mengelola keuangan Anda.</p>




                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 mt-5 mb-5">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Masuk</h3>
                            <?php if (isset($error)) : ?>
                            <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                                Alamat surel atau kata sandi yang Anda masukkan <strong>salah.</strong>
                                <button type="button" class="btn btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                            <?php endif; ?>
                            <form class="mt-3" action="" method="post">
                                <div class="mb-3">
                                    <label for="email_user" class="form-label">Alamat surel</label>
                                    <input type="email" class="form-control" id="email_user" name="email_user"
                                        aria-describedby="emailHelp" autofocus required placeholder="desta@domain.com">
                                </div>
                                <div class="mb-3">
                                    <label for="pass_user" class="form-label">Kata sandi</label>
                                    <input type="password" class="form-control" id="pass_user" name="pass_user" required
                                        placeholder="Kata sandi Anda">
                                </div>
                                <div class="d-grid gap-2">
                                    <button class="btn btn-primary" type="submit" name="submit">Masuk</button>
                                </div>
                            </form>
                            <p class="card-text mt-5 text-center">Tidak punya akun? <a href="signUp.php"
                                    class="link-primary"><strong>Daftar</strong></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
        var myModal = new bootstrap.Modal(document.getElementById('privacyPolicyModal'));
    });
    </script>

</html>